<?php
/**
 * The template used for displaying page content in template-homepage.php
 *
 * @package storefront
 */

?>
<?php
$featured_image = get_the_post_thumbnail_url( get_the_ID(), 'thumbnail' );
?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="<?php storefront_homepage_content_styles(); ?>"
	data-featured-image="<?php echo esc_url( $featured_image ); ?>">
	<div class="col-full">
		<?php
		/**
		 * Functions hooked in to storefront_page add_action
		 *
		 * @hooked storefront_homepage_header      - 10
		 * @hooked storefront_page_content         - 20
		 */
		// do_action( 'storefront_homepage' );
		?>
	</div>
</div><!-- #post-## -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
</script>


<section>

<video autoplay muted loop id="myVideo">
  <source src="<?php echo get_template_directory_uri(); ?>/assets/images/inicial.mp4" type="video/mp4">
  Your browser does not support HTML5 video.
</video>
</section>

<section id="about">

<div class="w3-animate-opacity title__section3">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/ABOUT.png" alt="Nueos Ingresos">
</div>

<div class="w3-animate-opacity title__section4">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/US2.gif" alt="Nueos Ingresos">
</div>

<div class="w3-animate-opacity title__section5">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/texto.png" alt="Nueos Ingresos">
</div>


</section>


<section id="smart">

<div class="w3-animate-opacity title__section6">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/SMART45.gif" alt="Nueos Ingresos">
</div>


</section>



<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
	<div class="row">
	<div class="col-3"><?php echo do_shortcode( '[popup_anything id="321"]' ); ?></div>
			<div class="col-3"><?php echo do_shortcode( '[popup_anything id="321"]' ); ?></div>
			<div class="col-3"><?php echo do_shortcode( '[popup_anything id="321"]' ); ?></div>
			<div class="col-3"><?php echo do_shortcode( '[popup_anything id="321"]' ); ?></div>
		</div>
    </div>
    <div class="carousel-item">
	<div class="row">
	<div class="col-3"><?php echo do_shortcode( '[popup_anything id="321"]' ); ?></div>
			<div class="col-3"><?php echo do_shortcode( '[popup_anything id="321"]' ); ?></div>
			<div class="col-3"><?php echo do_shortcode( '[popup_anything id="321"]' ); ?></div>
			<div class="col-3"><?php echo do_shortcode( '[popup_anything id="321"]' ); ?></div>
		</div>
    </div>
    <div class="carousel-item">
	<div class="row">
			<div class="col-3"><?php echo do_shortcode( '[popup_anything id="321"]' ); ?></div>
			<div class="col-3"><?php echo do_shortcode( '[popup_anything id="321"]' ); ?></div>
			<div class="col-3"><?php echo do_shortcode( '[popup_anything id="321"]' ); ?></div>
			<div class="col-3"><?php echo do_shortcode( '[popup_anything id="321"]' ); ?></div>
		</div>
    </div>
  </div>
</div>

<section id="cliente">
<div class="w3-animate-opacity title__section8">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/clientefont.png" alt="Nueos Ingresos">
</div>
<div class="w3-animate-opacity title__section7">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/GIF_Clientes.gif" alt="Nueos Ingresos">
</div>

</section>
<video autoplay muted loop id="myVideo3">
  <source src="<?php echo get_template_directory_uri(); ?>/assets/images/contacto.mp4" type="video/mp4">
  Your browser does not support HTML5 video.
</video>


	  <!--
	  
	  
<section id="inicio">

</section>

<section id="about">

</section>


<video autoplay muted loop id="myVideo2">
  <source src="<?php echo get_template_directory_uri(); ?>/assets/images/SMART.mp4" type="video/mp4">
  Your browser does not support HTML5 video.
</video>

<section id="smart">

</section>

<section id="proyectos">

</section>

<section id="cliente">

</section>


<video autoplay muted loop id="myVideo3">
  <source src="<?php echo get_template_directory_uri(); ?>/assets/images/contacto.mp4" type="video/mp4">
  Your browser does not support HTML5 video.
</video>

<section id="contacto2">

</section>-->

	
	
	

